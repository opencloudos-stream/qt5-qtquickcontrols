%global qt_module qtquickcontrols
%global __provides_exclude_from ^%{_qt5_archdatadir}/qml/.*\\.so$
%global majmin %%(echo %{version} | cut -d. -f1-2)

Name:    qt5-%{qt_module}
Summary: Qt5 - module with set of QtQuick controls
Version: 5.15.11
Release: 3%{?dist}
License: LGPLv2 or LGPLv3 and GFDL
Url:     http://www.qt.io
Source0: https://download.qt.io/official_releases/qt/%{majmin}/%{version}/submodules/%{qt_module}-everywhere-opensource-src-%{version}.tar.xz

BuildRequires: make, qt5-qtdeclarative-devel, qt5-qtbase-private-devel, qt5-qtbase-static >= %{version}, qt5-qtbase-devel >= %{version}
%{?_qt5:Requires: %{_qt5} = %{_qt5_version}}

%description
The Qt Quick Controls module provides a set of controls that can be used to
build complete interfaces in Qt Quick.



%prep
%autosetup -n %{qt_module}-everywhere-src-%{version} -p1



%build
%{qmake_qt5}

%make_build



%install
make install INSTALL_ROOT=%{buildroot}



%files
%license LICENSE.*
%{_qt5_archdatadir}/qml/QtQuick/
%{_qt5_examplesdir}/



%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 5.15.11-3
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 5.15.11-2
- Rebuilt for loongarch release

* Wed Jan 10 2024 edwardewang <edwardewang@tencent.com> - 5.15.11-1
- Upgrade to 5.15.11

* Thu Sep 21 2023 kianli <kianli@tencent.com> - 5.15.10-1
- Upgrade to 5.15.10

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 5.15.9-2
- Rebuilt for OpenCloudOS Stream 23.09

* Mon Jul 17 2023 Wang Guodong <gordonwwang@tencent.com> - 5.15.9-1
- Upgrade to 5.15.9

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 5.15.7-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 5.15.7-2
- Rebuilt for OpenCloudOS Stream 23

* Thu Dec 8 2022 Zhao Zhen <jeremiazhao@tencent.com> - 5.15.7-1
- initial
